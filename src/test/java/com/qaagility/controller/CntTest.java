package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;


public class CntTest {
    @Test
	public void testMethodA() throws Exception {
		int k = new Cnt().methodA(1, 1);{
			assertEquals("Division", 1, k);

		}	

	}
	@Test
        public void testMethodAIf() throws Exception {
                int k = new Cnt().methodA(1, 0);{
                        assertEquals("Max value", Integer.MAX_VALUE, k);

                }

        }

}
